require 'chronic_duration'
require 'http'
require 'gitlab'
require 'json'
require 'optimist'
require 'ruby-progressbar'
require 'tty-spinner'
require 'table_print'

class ReportGenerator
  attr_reader :project

  def initialize(environment_url, before_date, after_date)
    @gitlab_client = Gitlab.client(
      endpoint: "#{environment_url}/api/v4",
      private_token: ENV['ACCESS_TOKEN']
    )
    @before_date = before_date
    @after_date = after_date
  end

  def get_user_events(target_type: nil, action:)
    custom_params = { action: action, before: @before_date, after: @after_date }
    custom_params[:target_type] = target_type unless target_type.nil?
    get_all_entities(api_url: "#{@gitlab_client.endpoint}/events", custom_params: { target_type: target_type, action: action, before: @before_date, after: @after_date })
  end

  def prep_url_for_mr(mrs)
    mrs_projects = mrs.uniq { |mr| mr["project_id"] }.map { |mr| mr["project_id"] }
    projects_url = {}
    mrs_projects.each do |mr_proj|
      puts '.'
      web_url = make_http_request(method: 'get', url: "#{@gitlab_client.endpoint}/projects/#{mr_proj}")[:body]["web_url"]
      projects_url[mr_proj] = web_url
    end

    mrs.each do |mr|
      mr["web_url"] = "#{projects_url[mr['project_id']]}/merge_requests/#{mr['target_iid']}"
    end
  end

  def prep_url_for_issues(issues)
    i_projects = issues.uniq { |i| i["project_id"] }.map { |i| i["project_id"] }
    projects_url = {}
    i_projects.each do |i_proj|
      puts '*'
      web_url = make_http_request(method: 'get', url: "#{@gitlab_client.endpoint}/projects/#{i_proj}")[:body]["web_url"]
      projects_url[i_proj] = web_url
    end

    issues.each do |issue|
      issue["web_url"] = "#{projects_url[issue['project_id']]}/issues/#{issue['target_iid']}"
    end
  end

  def prep_comments(comments)
    comments.uniq! { |c| c["target_title"] }
    comments.each do |comment|
      comment["content"] = comment["note"]["body"].delete("@")
      comment["type"] = comment["note"]["noteable_type"]
    end
    comments.sort_by { |c| c["type"] }
  end

  ########## Requests ###########

  def make_http_request(method: 'get', url: nil, params: {}, headers: {}, fail_on_error: true)
    headers = { 'PRIVATE-TOKEN': ENV['ACCESS_TOKEN'] }.merge(headers)
    raise "URL not defined for making request. Exiting..." unless url

    res = HTTP.follow.method(method).call(url, form: params, headers: headers)

    raise "#{method.upcase} request failed!\nCode: #{res.code}\nResponse: #{res.body}\n" if fail_on_error && !res.status.success?

    { headers: res.headers, body: JSON.parse(res.body), status: res.status }
  end

  def get_all_entities(api_url:, custom_params:)
    per_page = 100
    headers = make_http_request(method: 'get', url: api_url, params: custom_params.merge({ page: 1, per_page: per_page }))[:headers]
    total_entity_pages = headers["X-Total-Pages"].to_i
    total_entities = headers["X-Total"].to_i
    spinner = TTY::Spinner.new("[:spinner] Collecting #{total_entities} #{api_url.split('/').last}")
    spinner.auto_spin
    entities = []
    1.upto(total_entity_pages) do |page_no|
      entity_response = make_http_request(method: 'get', url: api_url, params: custom_params.merge({ page: page_no, per_page: per_page }))
      entities.concat(entity_response[:body])
    end
    spinner.success

    entities
  end
end
