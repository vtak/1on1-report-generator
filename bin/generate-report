#!/usr/bin/env ruby

$LOAD_PATH.unshift File.expand_path('../lib', __dir__)
$stdout.sync = true

require 'chronic_duration'
require 'date'
require 'report_generator'
require 'gitlab'
require 'optimist'
require 'rainbow'
require 'table_print'
require 'uri'

opts = Optimist.options do
  banner "\nGenerate 1on1 Report for User."
  banner "Usage: generate-report [options]"
  banner "\nOptions:"
  opt :environment_url, "The environment's full URL", short: :none, type: :string, default: "https://gitlab.com"
  opt :before_date, "Include only events created before a particular date. Default: Date.today. Format: YYYY-MM-DD", type: :string, default: Date.today.strftime("%Y-%m-%d")
  opt :after_date, "Include only events created after a particular date. Default: (Date.today - 8 days). Format: YYYY-MM-DD", type: :string, default: (Date.today - 8).strftime("%Y-%m-%d")
  opt :help, 'Show help message'
  banner "\nEnvironment Variables:"
  banner "  ACCESS_TOKEN             A valid GitLab Personal Access Token for the specified environment. The token should come from a User that has API permission. (Default: nil)"
  banner "\nExamples:"
  banner "  Generate report for user"
  banner "  ./bin/generate-report"
  banner "  Generate report for user events after specfic YYYY-MM-DD date"
  banner "  ./bin/generate-report --after-date 2022-12-25"
end

raise 'Environment Variable ACCESS_TOKEN must be set to proceed. See command help for more info' unless ENV['ACCESS_TOKEN']

report_generator = ReportGenerator.new(opts[:environment_url], opts[:before_date], opts[:after_date])
# Check that environment can be reached and that token is valid
headers = {
  'Authorization': "Bearer #{ENV['ACCESS_TOKEN']}"
}
puts "Checking that GitLab environment '#{opts[:environment_url]}' is available and that provided Access Token works..."
check_res = report_generator.make_http_request(method: 'get', url: URI.join(opts[:environment_url], '/api/v4/version').to_s, headers: headers)
raise "Environment check has failed:\n#{check_res[:status]} - #{check_res[:body]}" if check_res[:status].client_error? || check_res[:status].server_error?

version = check_res[:body].values.join(' ')
puts "Environment and Access Token check was successful - URL: #{opts[:environment_url]}, Version: #{version}\n\n"

start_time = Time.now.to_i
puts Rainbow("Starting to generate report for the user...").color(230, 83, 40)
begin
  mr_created = report_generator.get_user_events(target_type: 'merge_request', action: "created")
  mr_reviewed = report_generator.get_user_events(target_type: 'merge_request', action: "approved")
  issues_created = report_generator.get_user_events(target_type: 'issue', action: "created")
  issues_closed = report_generator.get_user_events(target_type: 'issue', action: "closed")
  commented = report_generator.get_user_events(action: "commented")

  tp.set(:max_width, 200)
  mr_created_table = TablePrint::Printer.table_print(report_generator.prep_url_for_mr(mr_created), [{ "target_title" => { display_name: :title } }, "web_url"])
  mr_reviewed_table = TablePrint::Printer.table_print(report_generator.prep_url_for_mr(mr_reviewed), [{ "target_title" => { display_name: :title } }, "web_url"])

  issues_created_table = TablePrint::Printer.table_print(report_generator.prep_url_for_issues(issues_created), [{ "target_title" => { display_name: :title } }, "web_url"])
  issues_closed_table = TablePrint::Printer.table_print(report_generator.prep_url_for_issues(issues_closed), [{ "target_title" => { display_name: :title } }, "web_url"])

  commented_table = TablePrint::Printer.table_print(report_generator.prep_comments(commented), [{ "target_title" => { display_name: :title } }, "type", "content"])
  report_content = <<~DOC
    # Report #{opts[:after_date]} - #{opts[:before_date]}
    ## Merge requests
    ### Created
    #{mr_created_table}
    ### Reviewed
    #{mr_reviewed_table}

    ## Issue
    ### Created
    #{issues_created_table}
    ### Closed
    #{issues_closed_table}

    ## Commented / Discussions
    #{commented_table}
  DOC
  end_time = Time.now.to_i
  FileUtils.mkdir_p("#{Dir.pwd}/reports")
  report_path = "reports/report-#{opts[:before_date]}-#{end_time}.md"
  File.open(report_path, 'a') do |out_file|
    out_file.puts report_content
  end
  run_time = ChronicDuration.output(end_time - start_time, format: :long)
  puts Rainbow("Data generation finished after #{run_time}").green
  puts "\n█ Report: #{report_path}"
rescue Interrupt
  warn Rainbow("\nCaught the interrupt. Stopping.").yellow
  exit
rescue StandardError => e
  warn Rainbow("\nData generation failed:\n #{e.message}\n #{e.backtrace}").red
end
